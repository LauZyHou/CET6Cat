from django.apps import AppConfig


class UniversalConfig(AppConfig):
    name = 'UNIVERSAL'
